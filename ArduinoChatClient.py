#!/usr/bin/env python
import rospy
from std_msgs.msg import String


def chatCallback(data):
    print(data)

if __name__ == '__main__':
    rospy.init_node('arduino_chat_client', anonymous=True)
    rospy.Subscriber('/chatter', String, chatCallback)
    rospy.spin()
