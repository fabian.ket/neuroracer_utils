#!/usr/bin/env python

import rospy
from cv_bridge import CvBridge
import abc

class CvBridgeHandler(object):
    '''
    Base class of the CvBridge handler, abstraction for creating
    image msgs and getting images from msgs
    '''

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.cv_bridge = CvBridge()

    @abc.abstractmethod
    def get_image(self, msg, desired_encoding='passthrough'):
        '''
        Gets an msg and returns the image

        :param msg: ROS image msg, which contains the image
        :param desired_encoding: String the desired encoding

        :return Mat cv2 image
        '''

        return

    @abc.abstractmethod
    def get_msg(self, image):
        '''
        Gets am image and returns the msg

        :param image: Mat the image, which the created will msg contain

        :return ROS image msg
        '''
        return

    @abc.abstractmethod
    def log(self, msg):
        '''
        Logs msg header and format/encoding information

        :param msg: ROS image msg which should be logged
        '''

        pass


class CvBridgeHandlerRaw(CvBridgeHandler):

    def get_image(self, msg, desired_encoding='passthrough'):
        return self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding=desired_encoding)

    def get_msg(self, image):
        return self.cv_bridge.cv2_to_imgmsg(image)

    def log(self, msg):
        rospy.loginfo(msg.header.seq)
        rospy.loginfo(msg.encoding)

class CvBridgeHandlerCompr(CvBridgeHandler):

    def get_image(self, msg, desired_encoding='passthrough'):
        return self.cv_bridge.compressed_imgmsg_to_cv2(msg, desired_encoding=desired_encoding)

    def get_msg(self, image):
        return self.cv_bridge.cv2_to_compressed_imgmsg(image)

    def log(self, msg):
        rospy.loginfo(msg.header.seq)
        rospy.loginfo(msg.format)
