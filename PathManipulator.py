#!/usr/bin/env python

import os

def go_x_dirs_back(path, count = 1):

    if os.path.isfile(path):
        path = os.path.dirname(os.path.abspath(path))

    if os.path.isdir(path):
        path = os.path.abspath(path)

    else:
        raise IOError('No such dir: {}'.format( path ))

    for i in xrange(count):
        path = os.path.dirname(path)

    return path

def _main():
    print ( go_x_dirs_back(__file__, 2) )

if __name__ == '__main__':
    _main()
