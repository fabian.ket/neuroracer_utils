#!/usr/bin/env python
import yaml


def load_yaml(path):
    '''
    Loads YAML file from the given path

    :param path: String path to the file (/path/to/file.yaml)

    :return Map<String, Object> if can be loaded else None
    '''

    result = None

    with open(path, 'r') as data:
        try:
            result = yaml.load(data)
        except yaml.YAMLError as exc:
            print(exc)

    return result
