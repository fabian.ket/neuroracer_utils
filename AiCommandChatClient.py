#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist


def chatCallback(data):
    print(data)

if __name__ == '__main__':
    rospy.init_node('arduino_chat_client', anonymous=True)
    rospy.Subscriber('/neuroracer_ai/output/drive_command', Twist, chatCallback)
    rospy.spin()
