Project for implementations which are used by neuroracer_robot and neuroracer_ai

The YamlLoader and the CvBridgeHandler are in use.

YamlLoader:
- Just a single function to load yaml files


CvBridgeHandler:
- Used to pack and unpack ROS raw or comressed image msgs
